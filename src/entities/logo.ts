import { Entity, EntitySettings, Spritesheet } from 'squaredjs'
import Game from '../game'

export default class Logo extends Entity {

  public dir = { x: 1, y: 1 }

  public width = 16
  public height = 16

  constructor(data: EntitySettings) {
    super(data)

    const spritesheet = new Spritesheet('assets/spritesheets/logo.png', 16, 16)
    this.addAnimation(spritesheet, 'idle', [0], 1)
  }

  public update(delta: number): void {
    super.update(delta)

    // Move the entity
    this.x += this.dir.x * delta * 50
    this.y += this.dir.y * delta * 50

    // Make it bounce against walls
    if (this.x < 0) { this.dir.x = 1 }
    if (this.x + this.width > Game.instance.width) { this.dir.x = -1 }
    if (this.y < 0) { this.dir.y = 1 }
    if (this.y + this.height > Game.instance.height) { this.dir.y = -1 }

    // Flip sprite depending on direction
    const logo = Entity.findByName('logo')!
    logo.flip.x = this.dir.x > 0

  }
}
